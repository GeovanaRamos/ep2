# Como rodar?
- Baixar a os arquivos que estão nesse repositório e abrir a pasta com esses arquivos no **Eclipse**
- Abrir o pacote *main* 
- Selecionar *AprendaQEE.java*
- *Run as Java Application*  
  
Obs: Confira se na pasta de bibliotecas *Referenced Libraries* existem as bibliotecas **JFreeChart**, referentes a criação de gráficos neste projeto. Caso não esteja, pegue os arquivos .jar que se encontram na pasta desse repositório, em */jfreechart-1.0.19/lib*, e adicione ao projeto. 

# Como utilizar?
Após dar *Run as Java Application* a interface do menu irá aparecer com as três opções de simulação. O usuário poderá selecionar qualquer uma delas. Para mudar os gráficos basta mudar qualquer valor de ângulo e amplitude em qualquer uma das simulações. Os resultados das potências irão aparecer imediatamente após as mudanças, nas suas respectivas designações.  
Na *Simulação de Distorção Harmônica* há uma leve diferença de funcionamento. O gráfico de componente fundamental funcionará como descrito acima, mas para fazer cálculos com harmônicas o usuário deve fornecer primeiro a quantidade e apertar em *Criar Harmônicas*. Desse modo, será criada a quantidade de gráficos especificada, em conjunto com seus seletores de dados. Estes seletores aplicam as mudanças nos resultados assim que seus valores são modificados.  
Para retornar ao menu basta minimizar ou fechar a janela de simulação aberta, podendo escolher uma nova simulação ou encerrar o programa.

###### Observações
- A interface da simulação selecionada pode demorar para aparecer em sua primeira utilização.
- Ao mudar os valores de ângulo a atualização dos gráficos é bem sutil.
- O triângulo de potências é melhor visualizado com valores altos.


# Critérios de avaliação
Aqui descreve-se a forma como os critérios foram atendidos:
1. O diagrama de classes se encontra na pasta *doc*
2. O MVC foi definido pelos pacotes: *models*, *views* e *controllers*
3. Interface gráfica foi feita com Java Swing, Java AWT e JFreeChart 
4. -Agregação: classes interface passam objeto da classe controlador para classe gráfico
    -Composição: entre classes *PotenciaFundamental* e *Harmonicas*  
5. Gráficos implementam interface *Grafico* e controladores implementam *Controlador*
6. Classes que testam classes models se encontram no pacote *testes*
7. Resultados batem com exemplos do protótipo não funcional
8. Exceções tratadas nas classes controladoras, na criação das interfaces
9. Comentários onde necessário
10. +100 commits
11. Gráficos atualizam assim que o usuário modifica **qualquer** valor de entrada



