package controllers;

import java.awt.EventQueue;

import views.InterfaceMenu;

public class ControladorMenu implements Controlador {
	
	//iniciar interface do menu em um novo thread
	@Override
	public void iniciar_interface() {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					InterfaceMenu frame = new InterfaceMenu();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
		
	}
	
	// inicia as simulações chamando o controlador de cada uma
	public void comecar_simulacao(int uc_simulacao) {
		if(uc_simulacao == 2) {
			ControladorPotenciaFundamental cpf = new ControladorPotenciaFundamental();
			cpf.iniciar_interface();
			
		} else if (uc_simulacao == 3) {
			ControladorDistorcaoHarmonica cdh = new ControladorDistorcaoHarmonica();
			cdh.iniciar_interface();
			
		} else if (uc_simulacao == 4) {
			ControladorPotenciaHarmonico cph = new ControladorPotenciaHarmonico();
			cph.iniciar_interface();
		}
		
	}
	
	
}
