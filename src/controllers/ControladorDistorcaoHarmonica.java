package controllers;

import java.awt.EventQueue;
import java.util.List;

import models.DistorcaoHarmonica;
import models.Pontos;
import views.InterfaceDistorcaoHarmonica;

public class ControladorDistorcaoHarmonica implements Controlador {
	DistorcaoHarmonica dh = new DistorcaoHarmonica();
	
	//iniciar interface da simulação 2 em um novo thread
	@Override
	public void iniciar_interface() {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					InterfaceDistorcaoHarmonica frame = new InterfaceDistorcaoHarmonica();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
			
	}
	
	// intermediadores de dados entre modelo e interface 
	public void mudar_ang_fase_comp_fund(int ang_fase_comp_fund) {
		dh.setAngFaseCompFund(ang_fase_comp_fund);
	}
	public void mudar_paridade(String paridade) {
		dh.setParidade(paridade);
	}
	public void criar_ordem_harmonica(int num_harmonicas) {
		
	}
	public void mudar_ang_harmonica(int index, int ang_fase) {
		dh.getOrdensHarmonicas().get(index).setAngFase(ang_fase);
		
	}
	public void mudar_ordem_harmonica(int index, int ordem ) {
		dh.getOrdensHarmonicas().get(index).setOrdem(ordem);
	}
	public void mudar_amp_harmonica(int index, int amplitude) {
		dh.getOrdensHarmonicas().get(index).setAmplitude(amplitude);
	}
	public void mudar_ordens_harmonicas(int num_harmonicas) {
		dh.setOrdensHarmonicas(num_harmonicas);
	}
	
	public void mudar_amplitude_comp_fund(int amplitude_comp_fund) {
		dh.setAmplitudeCompFund(amplitude_comp_fund);
	}
	
	public List<Pontos> pegar_pontos_com_fund() {
		return dh.resolver_comp_fund();
	}
	
	public List<Pontos> pegar_pontos_harmonicas(int index) {
		return dh.resolver_ordem_harmonica(index);
	}
	
	public List<Pontos> pegar_pontos_fourrier() {
		return dh.resolver_fourrier();
	}
	public String pegar_serie_fourrier() {
		return dh.getSerieFourrier();
	}

}
