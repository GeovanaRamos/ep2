package controllers;

import java.awt.EventQueue;
import java.util.List;

import models.Pontos;
import models.PotenciaFundamental;
import views.InterfacePotenciaFundamental;

public class ControladorPotenciaFundamental implements Controlador {
	
	PotenciaFundamental pf = new PotenciaFundamental();
	
	//iniciar interface da simulação 1 em um novo thread
	@Override
	public void iniciar_interface() {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					InterfacePotenciaFundamental frame = new InterfacePotenciaFundamental();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
			
	}
	
	// intermediadores de dados entre modelo e interface 
	public void mudar_ang_fase(int valor) {
		pf.setAnguloDeFase(valor);
	}
	public void mudar_amp_corr(int valor) {
		pf.setAmplitudeCorrente(valor);
	}
	public void mudar_amp_ten(int valor) {
		pf.setAmplitudeTensao(valor);
	}
	public void mudar_ang_def(int valor) {
		pf.setAnguloDefasagemCorrente(valor);
	}
	
	public List<Pontos> pegar_pontos_tensao(){
		return pf.resolver_onda_tensao();
	}
	
	public List<Pontos> pegar_pontos_corrente(){
		return pf.resolver_onda_corrente();
	}
	public List<Pontos> pegar_pontos_potencia() {
		return pf.resolver_potencia_instantanea();
	}
	
	public double pegar_potencia_ativa(){
		return pf.getPotenciaAtiva();
	}
	public double pegar_potencia_reativa(){
		return pf.getPotenciaReativa();
	}
	public double pegar_potencia_aparente(){
		return pf.getPotenciaAparente();
	}
	public double pegar_fator_de_potencia(){
		return pf.getFatorDePotencia();
	}
	

	
}
