package controllers;

import java.awt.EventQueue;
import java.util.List;

import models.Pontos;
import models.PotenciaHarmonico;
import views.InterfarcePotenciaHarmonico;

public class ControladorPotenciaHarmonico implements Controlador{
	PotenciaHarmonico ph = new PotenciaHarmonico();
	
	//inicia interface da simulação 3 em um novo thread
	@Override
	public void iniciar_interface() {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					InterfarcePotenciaHarmonico frame = new InterfarcePotenciaHarmonico();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
			
	}
	
	//intermediadores de dados entre modelo e interface
	public void mudar_ang_tensao(int valor) {
		ph.setAngTensao(valor);
	}
	public void mudar_amp_tensao(int valor) {
		ph.setAmpTensao(valor);
	}
	public void mudar_ang_corrente(int valor) {
		ph.setAngCorrente(valor);
	}
	public void mudar_amp_corrente(int valor) {
		ph.setAmpCorrente(valor);
	}
	public void mudar_ordem_corrente(int valor) {
		ph.setOrdemCorrente(valor);
	}
	
	
	public List<Pontos> pegar_pontos_tensao(){
		return ph.resolver_tensao();
	}
	
	public List<Pontos> pegar_pontos_corrente(){
		return ph.resolver_corrente_harmonica();
	}
	public List<Pontos> pegar_pontos_potencia() {
		return ph.resolver_potencia_instantanea();
	}
	
	public double pegar_potencia_distorcao(){
		return ph.getPotenciaDistorcao();
	}
	public double pegar_potencia_liquida(){
		return ph.getPotenciaLiquida();
	}
	


}
