package views;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JSpinner;
import java.awt.GridLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;

import org.jfree.chart.ChartPanel;

import controllers.ControladorPotenciaFundamental;

import javax.swing.BoxLayout;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import javax.swing.event.ChangeListener;
import javax.swing.event.ChangeEvent;

@SuppressWarnings("serial")
public class InterfacePotenciaFundamental extends JFrame {
	
	private JScrollPane jsp;
	private JPanel contentPane;
	private JPanel panel_linha1;
	private JPanel panel_linha2;
	private JPanel panel_linha3;
	private JPanel panel_linha4;
	private JPanel panel_g1;
	private JPanel panel_g2;
	private JPanel panel_g3;
	private JPanel panel_tripot;
	private JPanel panel1_amp_ang;
	private JPanel panel2_amp_ang;
	private JSpinner ang_fase_tensao;
	private JSpinner amp_tensao;
	private JSpinner ang_fase_corr;
	private JSpinner amp_corr;
	private JTextField res_ativa;
	private JTextField res_reativa;
	private JTextField res_aparente;
	private JTextField res_fator;
	
	
	ControladorPotenciaFundamental cpf = new ControladorPotenciaFundamental();
	GraficoPotenciaFundamental grafico1;
	GraficoPotenciaFundamental grafico2;
	GraficoPotenciaFundamental grafico3;
	PlanoCartesiano triangulo_de_potencias;

	public InterfacePotenciaFundamental() {
		
		
		setTitle("Simular Fluxo de Potência Fundamental");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE); // retorna ao frame do menu
		setBounds(100, 100, 1200, 720);
		setExtendedState(JFrame.MAXIMIZED_BOTH); // tela cheia
		setUndecorated(false);
		
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		GridBagLayout gbl_contentPane = new GridBagLayout(); // grid com 5 linhas
		gbl_contentPane.columnWidths = new int[]{1190, 0};
		gbl_contentPane.rowHeights = new int[]{176, 176, 0, 176, 176, 0};
		gbl_contentPane.columnWeights = new double[]{1.0, Double.MIN_VALUE};
		gbl_contentPane.rowWeights = new double[]{0.0, 0.0, 1.0, 0.0, 0.0, Double.MIN_VALUE};
		contentPane.setLayout(gbl_contentPane);
		
		int v=ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS; // define janela com barra de rolagem
	    int h=ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED; 
	    jsp=new JScrollPane(contentPane,v,h);
	    jsp.setPreferredSize(null);
	    jsp.setBounds(150,670,850,200);
	    setContentPane(jsp);
		
	    // panel da tensão       
		panel_linha1 = new JPanel();
		panel_linha1.setPreferredSize(new Dimension(389, 200));
		GridBagConstraints gbc_panel_linha1 = new GridBagConstraints();
		gbc_panel_linha1.fill = GridBagConstraints.BOTH;
		gbc_panel_linha1.insets = new Insets(0, 0, 5, 0);
		gbc_panel_linha1.gridx = 0;
		gbc_panel_linha1.gridy = 0;
		contentPane.add(panel_linha1, gbc_panel_linha1);
		panel_linha1.setLayout(new GridLayout(0, 2, 0, 0));
		
		// panel gráfico da tensão
		panel_g1 = new JPanel();
		panel_linha1.add(panel_g1);
		panel_g1.setLayout(new BoxLayout(panel_g1, BoxLayout.X_AXIS));
		
		//grafico da tensão
        grafico1 = new GraficoPotenciaFundamental(1,cpf); 
		ChartPanel painel_grafico1 = new ChartPanel(grafico1.getChart());
		panel_g1.add(painel_grafico1);
		
		//panel dos valores da tensão
		panel1_amp_ang = new JPanel();
		panel_linha1.add(panel1_amp_ang);
		panel1_amp_ang.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		
		JLabel lblAnguloDeFase = new JLabel("Ângulo de fase");
		panel1_amp_ang.add(lblAnguloDeFase);
		
		//spinner do ângulo da tensão (ps: listener no fim do código)
		ang_fase_tensao = new JSpinner();		
		panel1_amp_ang.add(ang_fase_tensao);
		ang_fase_tensao.setPreferredSize(new Dimension(70,25));
		
		JLabel lblAmplitude = new JLabel("Amplitude");
		panel1_amp_ang.add(lblAmplitude);
		
		//spinner da amplitude da tensão (ps: listener no fim do código)
		amp_tensao = new JSpinner();	
		amp_tensao.setPreferredSize(new Dimension(70,25));
		panel1_amp_ang.add(amp_tensao);
		
		// panel da corrente		
		panel_linha2 = new JPanel();
		panel_linha2.setPreferredSize(new Dimension(389, 200));
		GridBagConstraints gbc_panel_linha2 = new GridBagConstraints();
		gbc_panel_linha2.fill = GridBagConstraints.BOTH;
		gbc_panel_linha2.insets = new Insets(0, 0, 5, 0);
		gbc_panel_linha2.gridx = 0;
		gbc_panel_linha2.gridy = 1;
		contentPane.add(panel_linha2, gbc_panel_linha2);
		panel_linha2.setLayout(new GridLayout(0, 2, 0, 0));
		
		// panel gráfico da corrente
		panel_g2 = new JPanel();
		panel_linha2.add(panel_g2);
		panel_g2.setLayout(new BoxLayout(panel_g2, BoxLayout.X_AXIS));
		
		//gráfico da corrente
		grafico2 = new GraficoPotenciaFundamental(2,cpf);
		ChartPanel painel_grafico2 = new ChartPanel(grafico2.getChart());
		panel_g2.add(painel_grafico2);
		
		//valores da corrente
		panel2_amp_ang = new JPanel();
		panel_linha2.add(panel2_amp_ang);
		panel2_amp_ang.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		
		JLabel lblnguloDeFase = new JLabel("Ângulo de fase");
		panel2_amp_ang.add(lblnguloDeFase);
		
		//spinner do ângulo da corrente (ps: listener no fim do código)
		ang_fase_corr = new JSpinner();
		ang_fase_corr.setPreferredSize(new Dimension(70,25));
		panel2_amp_ang.add(ang_fase_corr);
		
		JLabel label_2 = new JLabel("Amplitude");
		panel2_amp_ang.add(label_2);
		
		//spinner da amplitude da corrente (ps: listener no fim do código)
		amp_corr = new JSpinner();
		amp_corr.setPreferredSize(new Dimension(70,25));
		panel2_amp_ang.add(amp_corr);
		
		
		//panel potência instantânea
		panel_linha3 = new JPanel();
		GridBagConstraints gbc_panel_linha3 = new GridBagConstraints();
		gbc_panel_linha3.fill = GridBagConstraints.BOTH;
		gbc_panel_linha3.insets = new Insets(0, 0, 5, 0);
		gbc_panel_linha3.gridx = 0;
		gbc_panel_linha3.gridy = 3;
		contentPane.add(panel_linha3, gbc_panel_linha3);
		
		//panel gráfico da potência
		panel_g3 = new JPanel();
		panel_linha3.add(panel_g3);
		panel_g3.setLayout(new BoxLayout(panel_g3, BoxLayout.X_AXIS));
		
		// gráfico potência instantânea
		GraficoPotenciaFundamental grafico3 = new GraficoPotenciaFundamental(3,cpf);
		ChartPanel painel_grafico3 = new ChartPanel(grafico3.getChart());
		panel_g3.add(painel_grafico3);
		
		// panel triângulo de potências
		panel_tripot = new JPanel();
		panel_linha3.add(panel_tripot);
		
		// plano cartesiano para triângulo de potências
		triangulo_de_potencias = new PlanoCartesiano((int)cpf.pegar_potencia_ativa(), (int)cpf.pegar_potencia_reativa());
		panel_tripot.add(triangulo_de_potencias);
		triangulo_de_potencias.setPreferredSize(new Dimension(400, 400));
		
		// panel dos valores das potências
		panel_linha4 = new JPanel();
		GridBagConstraints gbc_panel_linha4 = new GridBagConstraints();
		gbc_panel_linha4.fill = GridBagConstraints.BOTH;
		gbc_panel_linha4.gridx = 0;
		gbc_panel_linha4.gridy = 4;
		contentPane.add(panel_linha4, gbc_panel_linha4);
		panel_linha4.setLayout(new GridLayout(4, 2, 0, 0));
		
		JLabel label_1 = new JLabel("Potência ativa");
		panel_linha4.add(label_1);
		
		res_ativa = new JTextField();
		panel_linha4.add(res_ativa);
		res_ativa.setColumns(10);
		res_ativa.setEditable(false);
		
		
		JLabel lblPotnciaReativa = new JLabel("Potência reativa");
		panel_linha4.add(lblPotnciaReativa);
		
		res_reativa = new JTextField();
		panel_linha4.add(res_reativa);
		res_reativa.setColumns(10);
		res_reativa.setEditable(false);
		
		
		JLabel lblNewLabel = new JLabel("Potência aparente ");
		panel_linha4.add(lblNewLabel);
		
		res_aparente = new JTextField();
		panel_linha4.add(res_aparente);
		res_aparente.setColumns(10);
		res_aparente.setEditable(false);
		
		
		JLabel lblNewLabel_1 = new JLabel("Fator de potência");
		panel_linha4.add(lblNewLabel_1);
		
		res_fator = new JTextField();
		panel_linha4.add(res_fator);
		res_fator.setColumns(10);
		res_fator.setEditable(false);
		
		
		//agora vou definir o que acontece quando o usuário muda o valor do ângulo da tensão
		ang_fase_tensao.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				atualizar_tensao();
				
			}
		});
		
		//agora vou definir o que acontece quando o usuário muda o valor da amplitude da tensão
		amp_tensao.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				atualizar_tensao();
			}
		});
		
		//agora vou definir o que acontece quando o usuário muda o valor do ângulo da corrente
		ang_fase_corr.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				atualizar_corrente();
			}
		});
		
		//agora vou definir o que acontece quando o usuário muda o valor da amplitude da corrente
		amp_corr.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				atualizar_corrente();
			}
		});
		
		
	}
	
	private void atualizar_tensao() {
		//pegando valores dos spinners
		int amp_ten = (int) amp_tensao.getValue();
		int ang_ten = (int) ang_fase_tensao.getValue();
		
		// mensagem de erro para valores fora dos limites
		if(amp_ten < 0 || amp_ten > 220 || ang_ten < -180 || ang_ten > 180 ) {
			JOptionPane.showMessageDialog(null, "Valores inválidos\nTensão: 0 ≤ Amplitude ≤ 220\n" + 
					"Ângulo: -180°≤ θ ≤ 180°" );
			return;
		}
		
		//vou pegar os dados da tensão e montar o novo gráfico
		cpf.mudar_amp_ten(amp_ten);
        cpf.mudar_ang_fase(ang_ten);
		GraficoPotenciaFundamental grafico_novo1 = new GraficoPotenciaFundamental(1,cpf);
		ChartPanel panel1 = new ChartPanel(grafico_novo1.getChart());
		atualizar_estado(panel_g1, panel1);
        
		atualizar_potencia();
		
	}
	
	private void atualizar_corrente() {
		int amp_cor = (int) amp_corr.getValue();
		int ang_cor = (int) ang_fase_corr.getValue();
		
		// mensagem de erro para valores fora dos limites
		if(amp_cor < 0 || amp_cor > 100 || ang_cor < -180 || ang_cor > 180) {
			JOptionPane.showMessageDialog(null, "Valores inválidos\n" + 
					"Corrente: 0 ≤ Amplitude ≤ 100\nÂngulo: -180°≤ θ ≤ 180°" );
			return;
		}
		
		//vou pegar os dados da corrente e montar o novo gráfico
        cpf.mudar_ang_def(ang_cor);		        
        cpf.mudar_amp_corr(amp_cor);
		GraficoPotenciaFundamental grafico_novo2 = new GraficoPotenciaFundamental(2,cpf);
        ChartPanel panel2 = new ChartPanel(grafico_novo2.getChart());
        atualizar_estado(panel_g2, panel2);
        
        atualizar_potencia();
    
	}
	
	private void atualizar_potencia() {   
        //com os dados atualizados agora eu atualizo o gráfico da potência
        GraficoPotenciaFundamental grafico_novo3 = new GraficoPotenciaFundamental(3,cpf);
        ChartPanel panel3 = new ChartPanel(grafico_novo3.getChart());
        atualizar_estado(panel_g3, panel3);
        
        // calculo as potências resultantes
        res_ativa.setText(Double.toString(cpf.pegar_potencia_ativa()) + " WATT");
        res_reativa.setText(Double.toString(cpf.pegar_potencia_reativa()) + " VAR");
        res_aparente.setText(Double.toString(cpf.pegar_potencia_aparente()) + " VA");
        res_fator.setText(Double.toString(cpf.pegar_fator_de_potencia()));
        
        // e atualizo o triangulo de potências
        PlanoCartesiano triangulo_de_potencias_novo = new PlanoCartesiano((int)cpf.pegar_potencia_ativa(), (int)cpf.pegar_potencia_reativa());
		triangulo_de_potencias_novo.setPreferredSize(new Dimension(400, 400));
		atualizar_estado(panel_tripot, triangulo_de_potencias_novo);
	}
	
	private void atualizar_estado(JPanel panel, Component comp) {
		panel.removeAll();
		panel.invalidate();								
        panel.add(comp);
        panel.validate();
        panel.repaint();
	}
	
	
}
