package views;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import controllers.ControladorMenu;

import javax.swing.JButton;
import javax.swing.SwingConstants;
import javax.swing.JLabel;
import javax.swing.Box;
import javax.swing.BoxLayout;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Component;
import java.awt.Dimension;

@SuppressWarnings("serial")
public class InterfaceMenu extends JFrame {

	private JPanel contentPane;
	ControladorMenu cm = new ControladorMenu();

	public InterfaceMenu() {
		
		
		setTitle("Aprenda QEE");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		setExtendedState(JFrame.MAXIMIZED_BOTH); 
		setUndecorated(false);
		
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new BoxLayout(contentPane, BoxLayout.Y_AXIS));
		
		JLabel lblAprendaQee = new JLabel("Escolha a simulação");
		lblAprendaQee.setAlignmentX(Component.CENTER_ALIGNMENT);
		lblAprendaQee.setHorizontalAlignment(SwingConstants.CENTER);
		contentPane.add(lblAprendaQee);
		
		//separaando label e primeiro botão
		contentPane.add(Box.createRigidArea(new Dimension(40,40)));
		
		JButton btnSimPotFund = new JButton("Simular Fluxo de Potência Fundamental");
		btnSimPotFund.setAlignmentX(Component.CENTER_ALIGNMENT);
		btnSimPotFund.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// chamando controlador de potência fundamental
				cm.comecar_simulacao(2);
			}
		});
		contentPane.add(btnSimPotFund);
		
		// separando primeiro e segundo botão
		contentPane.add(Box.createRigidArea(new Dimension(20,10)));
		
		JButton btnSimDis = new JButton("Simular Distorção Harmônica");
		btnSimDis.setAlignmentX(Component.CENTER_ALIGNMENT);
		btnSimDis.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// chamando o controlador da distorção
				cm.comecar_simulacao(3);
				
			}
		});
		contentPane.add(btnSimDis);
		
		// separando segundo e terceiro botão
		contentPane.add(Box.createRigidArea(new Dimension(20,10)));
		
		JButton btnSimPotHar = new JButton("Simular Fluxo de Potência Harmônico");
		btnSimPotHar.setAlignmentX(Component.CENTER_ALIGNMENT);
		btnSimPotHar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// chamando controlador de potência harmônico 
				cm.comecar_simulacao(4);
			}
		});
		contentPane.add(btnSimPotHar);
	}
}
