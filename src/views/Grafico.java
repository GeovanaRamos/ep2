package views;

import org.jfree.chart.JFreeChart;
import org.jfree.data.xy.XYDataset;

public interface Grafico {

	public void criar_grafico();
	public String criar_titulo();
	public String criar_eixoY();
	public XYDataset criar_dataset();
	public JFreeChart getChart();
}
