package views;

import java.util.ArrayList;
import java.util.List;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.renderer.xy.XYSplineRenderer;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

import controllers.ControladorDistorcaoHarmonica;
import models.Pontos;

public class GraficoDistorcaoHarmonica implements Grafico{
	private JFreeChart chart;
	private XYDataset dataset;
	private int num_grafico;
	private int index;
	ControladorDistorcaoHarmonica cdh;
	
	
	public GraficoDistorcaoHarmonica( int num_grafico, ControladorDistorcaoHarmonica cdh) {
		this.num_grafico = num_grafico;
		this.cdh = cdh;
		criar_grafico();
	}
	
	public GraficoDistorcaoHarmonica( int num_grafico, ControladorDistorcaoHarmonica cdh, int index) {
		this.num_grafico = num_grafico;
		this.cdh = cdh;
		this.index = index;
		criar_grafico();
	}
	
	@Override
	public void criar_grafico() {	
	    dataset = criar_dataset();
	    chart = ChartFactory.createXYLineChart(
	        criar_titulo(),
	        "Tempo",
	        criar_eixoY(),
	        dataset,
	        PlotOrientation.VERTICAL,
	        true, true, false);
	    chart.getXYPlot().setRenderer(new XYSplineRenderer());
	}
	
	@Override
	public String criar_titulo() {
		String titulo;
		switch (num_grafico) {
		case 1:
			titulo = "Componente Fundamental";
			break;
		case 2:
			titulo = "Harmônicas";
			break;
		case 3:
			titulo = "Resultante";
			break;
		default:
			titulo = " ";
			break;
		}
		return titulo;
	}
	
	@Override
	public String criar_eixoY() {
		String eixoY = "";
	
		return eixoY;
	}
	
	@Override
	public XYDataset criar_dataset() {
		  
		XYSeriesCollection dataset = new XYSeriesCollection();
	    XYSeries series = null;
	    List<Pontos> pontos = new ArrayList<Pontos>();
	    
	    switch( num_grafico ){
	    	case 1:
	    		series = new XYSeries("Componente fundamental");    
	    		pontos = cdh.pegar_pontos_com_fund();    		    
	    		for (int i = 0; i < pontos.size(); i++) {
	    			double x = pontos.get(i).getX();
	    			double y = pontos.get(i).getY();
	    			series.add(x,y);
	    			//System.out.println(x + " " + y);	
	    		}	                
	    		break;
	        
	        case 2:
	        	series = new XYSeries("Ordens Harmônicas");    
	    		pontos = cdh.pegar_pontos_harmonicas(index);	    		    
	    		for (int i = 0; i < pontos.size(); i++) {
	    			double x = pontos.get(i).getX();
	    			double y = pontos.get(i).getY();
	    			series.add(x,y);
	    			//System.out.println(x + " " + y);	
	    		}	                
	                
	            break;
	        
	        case 3:
	        	series = new XYSeries("Série de Fourrier");    
	    		pontos = cdh.pegar_pontos_fourrier();	    
	    		for (int i = 0; i < pontos.size(); i++) {
	    			double x = pontos.get(i).getX();
	    			double y = pontos.get(i).getY();
	    			series.add(x,y);
	    			//System.out.println(x + " " + y);	
	    		}
	                
	            break;
	        
	        default:
	        	
	                
	    }
	   

	    //Add series to dataset
	    dataset.addSeries(series);
	    
	    return dataset;
	}
	
	@Override
	public JFreeChart getChart() {
		return chart;
	}
	
	
}
