package views;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.JTextArea;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.EmptyBorder;

import org.jfree.chart.ChartPanel;

import controllers.ControladorDistorcaoHarmonica;

import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import javax.swing.BoxLayout;
import java.awt.GridLayout;
import javax.swing.JRadioButton;
import javax.swing.SwingConstants;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.event.ChangeListener;
import javax.swing.text.DefaultCaret;
import javax.swing.event.ChangeEvent;

@SuppressWarnings("serial")
public class InterfaceDistorcaoHarmonica extends JFrame {

	private JPanel contentPane;
	private JScrollPane jsp;
	private JPanel panel_linha1;
	private JPanel panel_linha2;
	private JPanel panel_linha3;
	private JPanel panel_linha4;
	private JPanel panel_g1;
	private JPanel panel_g3;
	private JPanel panel1_ang;
	private JPanel panel_harmonicos;
	private JPanel panel_rdiobtn;
	private JPanel panel_fourrier;
	private JSpinner ang_fase_fund;
	private JSpinner amp_fund;
	private JSpinner spinner_harmonicos;
	private JRadioButton rdbtnNewRadioButton;
	private JRadioButton rdbtnNewRadioButton_1;
	private JTextArea lbl_fourrier;
	
	private JPanel[] panel_g2;
	private JPanel[] panel2_ang;
	private JLabel[] lbl_ang;
	private JLabel[] lbl_amp;
	private JLabel[] lbl_ord_har;
	private JSpinner[] spinner_amp_harm; 
	private JSpinner[] spinner_ang_harm ;
	private JSpinner[] spinner_ord_har;
	private ChartPanel[] painel_grafico2;
	
	
	ControladorDistorcaoHarmonica cdh = new ControladorDistorcaoHarmonica();
	GraficoDistorcaoHarmonica grafico1;
	GraficoDistorcaoHarmonica grafico3;

	public InterfaceDistorcaoHarmonica() {
		
		setTitle("Simular Distorção Harmônica");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE); // retorna ao frame do menu
		setBounds(100, 100, 1200, 700);
		setExtendedState(JFrame.MAXIMIZED_BOTH); // tela cheia
		setUndecorated(false);
				
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		
		int v=ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS;// barra de rolagem
	    int h=ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED; 
	    jsp=new JScrollPane(contentPane,v,h);
	    setContentPane(jsp);
	    GridBagLayout gbl_contentPane = new GridBagLayout();
	    gbl_contentPane.columnWidths = new int[]{0, 0};
	    gbl_contentPane.rowHeights = new int[]{0, 0, 0, 0, 0, 0, 0};
	    gbl_contentPane.columnWeights = new double[]{1.0, Double.MIN_VALUE};
	    gbl_contentPane.rowWeights = new double[]{0, 0, 1.0, 1.0, 1.0, 1.0, Double.MIN_VALUE};
	    contentPane.setLayout(gbl_contentPane);
	    
	    //panel do componente fundamental
	    panel_linha1 = new JPanel();
	    panel_linha1.setPreferredSize(new Dimension(389, 200));
	    panel_linha1.setMinimumSize(new Dimension(388, 200));
	    GridBagConstraints gbc_panel_linha1 = new GridBagConstraints();
	    gbc_panel_linha1.insets = new Insets(0, 0, 5, 0);
	    gbc_panel_linha1.fill = GridBagConstraints.BOTH;
	    gbc_panel_linha1.gridx = 0;
	    gbc_panel_linha1.gridy = 0;
	    contentPane.add(panel_linha1, gbc_panel_linha1);
	    panel_linha1.setLayout(new GridLayout(0, 2, 0, 0));
	    
	    //panel do gráfico componente fundamental
	    panel_g1 = new JPanel();
	    panel_linha1.add(panel_g1);
	    
	    grafico1 = new GraficoDistorcaoHarmonica(1, cdh);
	    panel_g1.setLayout(new BoxLayout(panel_g1, BoxLayout.X_AXIS));
	    ChartPanel painel_grafico1 = new ChartPanel(grafico1.getChart());
		panel_g1.add(painel_grafico1);
	    
		//panel dos dados do componente fundamental
		panel1_ang = new JPanel();
		panel_linha1.add(panel1_ang);
		panel1_ang.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		
		JLabel lblAnguloDeFase = new JLabel("Ângulo de fase");
		panel1_ang.add(lblAnguloDeFase);
		
		//spinner ângulo fundamental (ps: listener no final do código)
	    ang_fase_fund = new JSpinner();  
		panel1_ang.add(ang_fase_fund);
		ang_fase_fund.setPreferredSize(new Dimension(70,25));
		
		JLabel lblAmplitude = new JLabel("Amplitude");
		panel1_ang.add(lblAmplitude);
		
		//spinnner amplitude fundamental (ps: listener no final do código)
		amp_fund = new JSpinner();
		amp_fund.setPreferredSize(new Dimension(70,25));
		panel1_ang.add(amp_fund);
	    
		// panel dos dados harmônicos
	    panel_linha2 = new JPanel();
	    panel_linha2.setPreferredSize(new Dimension(389, 80));
	    GridBagConstraints gbc_panel_linha2 = new GridBagConstraints();
	    gbc_panel_linha2.insets = new Insets(0, 0, 5, 0);
	    gbc_panel_linha2.fill = GridBagConstraints.BOTH;
	    gbc_panel_linha2.gridx = 0;
	    gbc_panel_linha2.gridy = 1;
	    contentPane.add(panel_linha2, gbc_panel_linha2);
	    GridBagLayout gbl_panel_linha2 = new GridBagLayout();
	    gbl_panel_linha2.columnWidths = new int[] {636, 636};
	    gbl_panel_linha2.rowHeights = new int[] {0, 30};
	    gbl_panel_linha2.columnWeights = new double[]{1.0, 1.0};
	    gbl_panel_linha2.rowWeights = new double[]{1.0, 0.0};
	    panel_linha2.setLayout(gbl_panel_linha2);
	   
	    panel_harmonicos = new JPanel();
	    panel_harmonicos.setPreferredSize(new Dimension(144, 20));
	    GridBagConstraints gbc_panel_harmonicos = new GridBagConstraints();
	    gbc_panel_harmonicos.insets = new Insets(0, 0, 5, 0);
	    gbc_panel_harmonicos.fill = GridBagConstraints.BOTH;
	    gbc_panel_harmonicos.gridx = 1;
	    gbc_panel_harmonicos.gridy = 0;
	    panel_linha2.add(panel_harmonicos, gbc_panel_harmonicos);
	    panel_harmonicos.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
	    
	    JLabel label = new JLabel("Numero de ordens harmônincas");
	    panel_harmonicos.add(label);
	    
	    // botão criar_harmonicas principal expande o panel_linha3 
	    //com o número de gráficos selecionado pelo usuário
	    //ps: listener no final do código
	    JButton btn_criar_harmonicas = new JButton("Criar harmônicas");
	    GridBagConstraints gbc_btn_criar_harmonicas = new GridBagConstraints();
	    gbc_btn_criar_harmonicas.gridx = 1;
	    gbc_btn_criar_harmonicas.gridy = 1;
	    panel_linha2.add(btn_criar_harmonicas, gbc_btn_criar_harmonicas);
	    
	    //panel que irá receber os gráficos de cada harmonico junto com seus recolhedores de dados
	    panel_linha3 = new JPanel();
	    panel_linha3.setMaximumSize(new Dimension(389,10000));
	    GridBagConstraints gbc_panel_linha3 = new GridBagConstraints();
	    gbc_panel_linha3.gridy = 2;
	    gbc_panel_linha3.insets = new Insets(0, 0, 5, 0);
	    gbc_panel_linha3.fill = GridBagConstraints.BOTH;
	    gbc_panel_linha3.gridx = 0;
	    contentPane.add(panel_linha3, gbc_panel_linha3);
	    panel_linha3.setLayout(new BoxLayout(panel_linha3, BoxLayout.Y_AXIS));
	    
	    spinner_harmonicos = new JSpinner();
	    spinner_harmonicos.setPreferredSize(new Dimension(70,25));
	    panel_harmonicos.add(spinner_harmonicos);
	    
	    JLabel label_1 = new JLabel("        Harmônicos");
	    panel_harmonicos.add(label_1);
	    
	    panel_rdiobtn = new JPanel();
	    panel_harmonicos.add(panel_rdiobtn);
	    panel_rdiobtn.setLayout(new BoxLayout(panel_rdiobtn, BoxLayout.Y_AXIS));
	    
	    rdbtnNewRadioButton = new JRadioButton("Ímpares");
	    panel_rdiobtn.add(rdbtnNewRadioButton);
	    
	    rdbtnNewRadioButton_1 = new JRadioButton("Pares");
	    panel_rdiobtn.add(rdbtnNewRadioButton_1);
	    
	    //ja defino antes o panel_linha4 para que seja atualizado pelos
		// componentes posteriormente adicionados ao panel_linha3
	    panel_linha4 = new JPanel();
		panel_linha4.setPreferredSize(new Dimension(389,200));
	    GridBagConstraints gbc_panel_linha4 = new GridBagConstraints();
	    gbc_panel_linha4.insets = new Insets(0, 0, 5, 0);
		gbc_panel_linha4.fill = GridBagConstraints.BOTH;
	    gbc_panel_linha4.gridx = 0;
	    gbc_panel_linha4.gridy = 3;
	    contentPane.add(panel_linha4, gbc_panel_linha4);
	    panel_linha4.setLayout(new GridLayout(1, 2, 0, 0));
	    
	    //panel do gráfico da série de fourrier
	    panel_g3 = new JPanel();
	    panel_g3.setLayout(new BoxLayout(panel_g3, BoxLayout.X_AXIS));
	    panel_linha4.add(panel_g3);
	    
	    //gráfico da série de fourrier
	    grafico3 = new GraficoDistorcaoHarmonica(3, cdh);
	   	ChartPanel painel_grafico3 = new ChartPanel(grafico3.getChart());
	   	panel_g3.add(painel_grafico3);
		    
	    panel_fourrier = new JPanel();
	    panel_linha4.add(panel_fourrier);
	    panel_fourrier.setLayout(new GridLayout(2, 1, 0, 0));
		    
	    JLabel lblNewLabel = new JLabel(" Série de Fourrier Amplitude-Fase");
	    lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
	    panel_fourrier.add(lblNewLabel);
		    
	    lbl_fourrier = new JTextArea("");
	    //defino a série de fourrier usando só com
	    //os dados do componente fundamental, por enquanto
	    lbl_fourrier.setText(cdh.pegar_serie_fourrier());
	    DefaultCaret caret = (DefaultCaret)lbl_fourrier.getCaret();
	    caret.setUpdatePolicy(DefaultCaret.NEVER_UPDATE);
	    panel_fourrier.add(lbl_fourrier);
	    

	    // aqui será definido o que acontece quando o usuário aperta criar harmônicos
	    btn_criar_harmonicas.addActionListener(new ActionListener() {
	    	public void actionPerformed(ActionEvent e) {
	    		
				int num_harmonicos = (int) spinner_harmonicos.getValue();
			
				// mensagem de erro para valores fora dos limites
				if( num_harmonicos < 0 || num_harmonicos > 6) {
					JOptionPane.showMessageDialog(null, "Valores inválidos\nNúmero de harmônicos: 0 ≤ n ≤ 6" );
					return;
				}
	        
		        //prepara o panel_linha3
		        panel_linha3.removeAll();
		        panel_linha3.invalidate();
		        
		        // inicializa variavéis
	    		panel_g2 = new JPanel[num_harmonicos];
	    		panel2_ang = new JPanel[num_harmonicos];
	    		lbl_ang = new JLabel[num_harmonicos];
	    		lbl_amp = new JLabel[num_harmonicos];
	    		lbl_ord_har = new JLabel[num_harmonicos];
	    		spinner_amp_harm = new JSpinner[num_harmonicos];
	    		spinner_ang_harm = new JSpinner[num_harmonicos];
	    		spinner_ord_har = new JSpinner[num_harmonicos];
	    		painel_grafico2 = new ChartPanel[num_harmonicos];
	    		
	    		//estabelece uma lista com n harmonicos, referente a cada gráfico
	    		cdh.mudar_ordens_harmonicas(num_harmonicos);
    		
	   		    //agora irei adicionar dinamicamente o número de harmônicos 
	   		    // selecionados pelo usuário
	    		for (int i = 0; i < num_harmonicos; i++) {
	    			
	    			// panel de cada gŕafico de harmônicas
	 	    	    panel_g2[i] = new JPanel();
	 	    	    panel_g2[i].setPreferredSize(new Dimension(289, 200));
	 	    	    panel_g2[i].setLayout(new BoxLayout(panel_g2[i], BoxLayout.Y_AXIS));
	 	    	    panel_linha3.add(panel_g2[i]);
	 	    	    
	 	    	    //gráfico da distorção
		 	   	    GraficoDistorcaoHarmonica grafico2 = new GraficoDistorcaoHarmonica(2, cdh,i);
		 	   	    painel_grafico2[i] = new ChartPanel(grafico2.getChart());
		 	   	    panel_g2[i].add(painel_grafico2[i]);
	 	    	    
		 	   	    //panel dos valores harmônicos
	 	    	    panel2_ang[i] = new JPanel();
	 	    	    panel_linha3.add(panel2_ang[i]);
	 	    	    panel2_ang[i].setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
	 	    	    
	 	    	    lbl_ang[i] = new JLabel("Ângulo de fase");
	 	    	    panel2_ang[i].add(lbl_ang[i]);
	 	    	    
	 	    	    spinner_ang_harm[i] = new JSpinner();
	 	    	    spinner_ang_harm[i].setPreferredSize(new Dimension(70,25));
	 	    	    panel2_ang[i].add(spinner_ang_harm[i]);
 	    	    
	 	    	    lbl_amp[i] = new JLabel("Amplitude");
	 	    	    panel2_ang[i].add(lbl_amp[i]);
	 	    	    
	 	    	    spinner_amp_harm[i] = new JSpinner();
	 	    	    spinner_amp_harm[i].setPreferredSize(new Dimension(70,25));
	 	    	    panel2_ang[i].add(spinner_amp_harm[i]);
	 	    	    
	 	    	    lbl_ord_har[i] = new JLabel("Ordem harmônica");
	 	    	    panel2_ang[i].add(lbl_ord_har[i]);
	 	    	    
	 	    	    spinner_ord_har[i] = new JSpinner();
	 	    	    spinner_ord_har[i].setPreferredSize(new Dimension(70,25));
	 	    	    panel2_ang[i].add(spinner_ord_har[i]);	 	    	  
	 	    	    
	 	    	    // necessário para reutilzar o "i" em uma classe mais interna
	 	    	    final Integer index = new Integer(i);
	 	    	    
	 	    	    //listeners
	 	    	    spinner_ang_harm[i].addChangeListener(new ChangeListener() {
		 		    	public void stateChanged(ChangeEvent e) {
		 		    		atualizar_harmonica(index);
		 		    	}
		 		    });
	 	    	    spinner_amp_harm[i].addChangeListener(new ChangeListener() {
		 		    	public void stateChanged(ChangeEvent e) {
		 		    		atualizar_harmonica(index);
		 		    	}
		 		    });
	 	    	    spinner_ord_har[i].addChangeListener(new ChangeListener() {
		 		    	public void stateChanged(ChangeEvent e) {
		 		    		atualizar_harmonica(index);
		 		    	}
		 		    });
  	   	 	    	    		 	    	    
				}
	    		
 	    	    panel_linha3.validate();
			    panel_linha3.repaint();
			    jsp.revalidate();      
	    		
	    	}
	    });
	    
	    
	    ang_fase_fund.addChangeListener(new ChangeListener() {
	    	public void stateChanged(ChangeEvent e) {
	    		atualizar_comp_fundamental();

	    	}
	    });
	    
	    amp_fund.addChangeListener(new ChangeListener() {
	    	public void stateChanged(ChangeEvent e) {
	    		atualizar_comp_fundamental();
	    	}
	    });

	    
	    
	    
	}
	
	private void atualizar_comp_fundamental() {
		//pegando valores dos spinners
		int amp_fundl  = (int) amp_fund.getValue();
		int ang_fund = (int) ang_fase_fund.getValue();
			
		// mensagem de erro para valores fora dos limites
		if(amp_fundl < 0 || amp_fundl > 220 || ang_fund < -180 || ang_fund > 180 ) {
			JOptionPane.showMessageDialog(null, "Valores inválidos\nFundamental: 0 ≤ Amplitude ≤ 220\n" +
					"Ângulo: -180°≤ θ ≤ 180°" );
			return;
		}	

    	// atualizar os dados do componente fundamental e depois montar o novo gráfico
		cdh.mudar_amplitude_comp_fund(amp_fundl);
   		cdh.mudar_ang_fase_comp_fund(ang_fund);	
		GraficoDistorcaoHarmonica grafico_novo = new GraficoDistorcaoHarmonica(1, cdh);
		ChartPanel panel = new ChartPanel(grafico_novo.getChart());	
	    atualizar_estado(panel_g1, panel);
	    
	    atualizar_fourrier();
		
	}
	private void atualizar_harmonica(int index) {

		//pegando valores dos spinners
		int amp_har  = (int)spinner_amp_harm[index].getValue();
		int ang_har = (int)spinner_ang_harm[index].getValue();
		int ord_har = (int)spinner_ord_har[index].getValue();

			
			// mensagem de erro para valores fora dos limites
		if(amp_har < 0 || amp_har > 220 || ang_har < -180 || ang_har > 180 ||
				ord_har < 0 || ord_har > 15) {
			JOptionPane.showMessageDialog(null, "Valores inválidos\nHarmônicas: 0 ≤ Amplitude ≤ 220\n" +
					"Ângulo: -180°≤ θ ≤ 180°\nOrdem Harmônica: 0 ≤ h ≤ 15" );
			return;
		}
 		//atualizar o gráfico de harmônicas
		cdh.mudar_ang_harmonica(index, ang_har);
 		cdh.mudar_amp_harmonica(index, amp_har);
 		cdh.mudar_ordem_harmonica(index, ord_har);
		GraficoDistorcaoHarmonica grafico_novo = new GraficoDistorcaoHarmonica(2, cdh, index);
	    ChartPanel panel = new ChartPanel(grafico_novo.getChart());	
	    atualizar_estado(panel_g2[index], panel);
	    
	    atualizar_fourrier();
	        
	}
	
	private void atualizar_fourrier() {
        //atualizar o gráfico da série de fourrier
        GraficoDistorcaoHarmonica grafico_novo3 = new GraficoDistorcaoHarmonica(3, cdh);
        ChartPanel panel3 = new ChartPanel(grafico_novo3.getChart());
        atualizar_estado(panel_g3, panel3);
	        
        //atualizar a série de fourrier
        lbl_fourrier.setText(cdh.pegar_serie_fourrier());
	}
	
	private void atualizar_estado(JPanel panel, Component comp) {
		panel.removeAll();
		panel.invalidate();								
        panel.add(comp);
        panel.validate();
        panel.repaint();
	}
}
