package views;

import java.util.ArrayList;
import java.util.List;


import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.renderer.xy.XYSplineRenderer;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

import controllers.ControladorPotenciaFundamental;
import models.Pontos;



public class GraficoPotenciaFundamental implements Grafico {
	private JFreeChart chart;
	private XYDataset dataset;
	private int num_grafico;
	ControladorPotenciaFundamental cpf;
	
	
	public GraficoPotenciaFundamental( int num_grafico, ControladorPotenciaFundamental cpf) {
		this.num_grafico = num_grafico;
		this.cpf = cpf;
		criar_grafico();
	}
	
	@Override
	public void criar_grafico() {	
	    dataset = criar_dataset();
	    chart = ChartFactory.createXYLineChart(
	        criar_titulo(),
	        "Tempo",
	        criar_eixoY(),
	        dataset,
	        PlotOrientation.VERTICAL,
	        true, true, false);
	    chart.getXYPlot().setRenderer(new XYSplineRenderer());
	}
	
	@Override
	public String criar_titulo() {
		String titulo;
		switch (num_grafico) {
		case 1:
			titulo = "Tensão";
			break;
		case 2:
			titulo = "Corrente";
			break;
		case 3:
			titulo = "Potência instantânea";
			break;
		default:
			titulo = " ";
			break;
		}
		return titulo;
	}
	
	@Override
	public String criar_eixoY() {
		String eixoY;
		switch (num_grafico) {
		case 1:
			eixoY = "Tensão";
			break;
		case 2:
			eixoY = "Corrente";
			break;
		case 3:
			eixoY = "Potência instantânea";
			break;
		default:
			eixoY = " ";
			break;
		}
		return eixoY;
	}
	
	@Override
	public XYDataset criar_dataset() {
		  
		XYSeriesCollection dataset = new XYSeriesCollection();
	    XYSeries series = null;
	    List<Pontos> pontos = new ArrayList<Pontos>();
	    
	    switch( num_grafico ){
	    	case 1:
	    		series = new XYSeries("Tensão");    
	    		pontos = cpf.pegar_pontos_tensao();	    		    
	    		for (int i = 0; i < pontos.size(); i++) {
	    			double x = pontos.get(i).getX();
	    			double y = pontos.get(i).getY();
	    			series.add(x,y);
	    			//System.out.println(x + " " + y);	
	    		}	                
	    		break;
	        
	        case 2:
	        	series = new XYSeries("Corrente");    
	    		pontos = cpf.pegar_pontos_corrente();	    		    
	    		for (int i = 0; i < pontos.size(); i++) {
	    			double x = pontos.get(i).getX();
	    			double y = pontos.get(i).getY();
	    			series.add(x,y);
	    			//System.out.println(x + " " + y);	
	    		}	                
	                
	            break;
	        
	        case 3:
	        	series = new XYSeries("Potência");    
	    		pontos = cpf.pegar_pontos_potencia();	    
	    		for (int i = 0; i < pontos.size(); i++) {
	    			double x = pontos.get(i).getX();
	    			double y = pontos.get(i).getY();
	    			series.add(x,y);
	    			//System.out.println(x + " " + y);	
	    		}
	                
	            break;
	        
	        default:
	        	
	                
	    }
	   
	    dataset.addSeries(series);
	    
	    return dataset;
	}
	
	@Override
	public JFreeChart getChart() {
		return chart;
	}
	
	
	

}
