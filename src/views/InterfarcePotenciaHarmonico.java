package views;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import org.jfree.chart.ChartPanel;

import controllers.ControladorPotenciaHarmonico;

import java.awt.GridBagLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.GridLayout;
import javax.swing.BoxLayout;
import java.awt.FlowLayout;
import javax.swing.JTextField;

@SuppressWarnings("serial")
public class InterfarcePotenciaHarmonico extends JFrame {

	private JScrollPane jsp;
	private JPanel contentPane;
	private JPanel panel_linha1;
	private JPanel panel_linha2;
	private JPanel panel_linha3;
	private JPanel panel_linha4;
	private JPanel panel_angamp1;
	private JPanel panel_angamp2;
	private JPanel panel_g1;
	private JPanel panel_g2;
	private JPanel panel_g3;
	private JPanel panel_potencias;
	private JSpinner ang_fase_tensao;
	private JSpinner amp_tensao;
	private JSpinner ang_fase_corrente;
	private JSpinner amp_corrente;
	private JSpinner ordem_har;
	private JTextField tfliquida;
	private JTextField tfdistorcao;
	private JTextField tftpf;
	
	ControladorPotenciaHarmonico cph = new ControladorPotenciaHarmonico();
	GraficoPotenciaHarmonico grafico1;
	GraficoPotenciaHarmonico grafico2;
	GraficoPotenciaHarmonico grafico3;

	
	public InterfarcePotenciaHarmonico() {
		
		setTitle("Simular Fluxo de Potência Harmônico");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE); // retorna ao frame do menu
		setBounds(100, 100, 1200, 720);
		setExtendedState(JFrame.MAXIMIZED_BOTH); // tela cheia
		setUndecorated(false);
		
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		
		int v=ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS;
	    int h=ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED; 
	    jsp=new JScrollPane(contentPane,v,h); // barra de rolagem
	    GridBagLayout gbl_contentPane = new GridBagLayout();
	    gbl_contentPane.columnWidths = new int[]{1273, 0};
	    gbl_contentPane.rowHeights = new int[]{200, 200, 0, 200, 0, 0};
	    gbl_contentPane.columnWeights = new double[]{1.0, Double.MIN_VALUE};
	    gbl_contentPane.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 1.0, Double.MIN_VALUE};
	    contentPane.setLayout(gbl_contentPane);
	    
	    // panel da tensão
	    panel_linha1 = new JPanel();
	    panel_linha1.setPreferredSize(new Dimension(389, 200));
	    GridBagConstraints gbc_panel_linha1 = new GridBagConstraints();
	    gbc_panel_linha1.fill = GridBagConstraints.BOTH;
	    gbc_panel_linha1.insets = new Insets(0, 0, 5, 0);
	    gbc_panel_linha1.gridx = 0;
	    gbc_panel_linha1.gridy = 0;
	    contentPane.add(panel_linha1, gbc_panel_linha1);
	    
	    //gráfico tensão
	    grafico1 = new GraficoPotenciaHarmonico(1, cph);
	    panel_linha1.setLayout(new GridLayout(0, 2, 0, 0));
	    panel_g1 = new JPanel();
	    panel_linha1.add(panel_g1);
	    panel_g1.setLayout(new BoxLayout(panel_g1, BoxLayout.X_AXIS));
	    ChartPanel painel_grafico1 = new ChartPanel(grafico1.getChart());
	    panel_g1.add(painel_grafico1);
	    
	    //panel valores da tensão
	    panel_angamp1 = new JPanel();
	    panel_linha1.add(panel_angamp1);
	    panel_angamp1.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
	    
	    JLabel lblAnguloDeFase = new JLabel("Ângulo de fase");
		panel_angamp1.add(lblAnguloDeFase);
		
		ang_fase_tensao = new JSpinner();
		panel_angamp1.add(ang_fase_tensao);
		ang_fase_tensao.setPreferredSize(new Dimension(70,25));
		
		JLabel lblAmplitude = new JLabel("Amplitude");
		panel_angamp1.add(lblAmplitude);
		
		amp_tensao = new JSpinner();		
		amp_tensao.setPreferredSize(new Dimension(70,25));
		panel_angamp1.add(amp_tensao);
	    
	    // panel da corrente	    
	    panel_linha2 = new JPanel();
	    panel_linha2.setPreferredSize(new Dimension(389, 200));
	    GridBagConstraints gbc_panel_linha2 = new GridBagConstraints();
	    gbc_panel_linha2.fill = GridBagConstraints.BOTH;
	    gbc_panel_linha2.insets = new Insets(0, 0, 5, 0);
	    gbc_panel_linha2.gridx = 0;
	    gbc_panel_linha2.gridy = 1;
	    contentPane.add(panel_linha2, gbc_panel_linha2);
	    panel_linha2.setLayout(new GridLayout(0, 2, 0, 0));
	    
	    // panel do gráfico da corrente
	    panel_g2 = new JPanel();
	    panel_linha2.add(panel_g2);
	    panel_g2.setLayout(new BoxLayout(panel_g2, BoxLayout.X_AXIS));
	    
	    //gráfico da corrente
	    grafico2 = new GraficoPotenciaHarmonico(2, cph);
	    ChartPanel painel_grafico2 = new ChartPanel(grafico2.getChart());
	    panel_g2.add(painel_grafico2);
	    
	    //panel dos valores da corrente
	    panel_angamp2 = new JPanel();
	    panel_linha2.add(panel_angamp2);
	    
	    JLabel lblAnguloDeFase2 = new JLabel("Ângulo de fase");
		panel_angamp2.add(lblAnguloDeFase2);
		
		ang_fase_corrente = new JSpinner();
		panel_angamp2.add(ang_fase_corrente);
		ang_fase_corrente.setPreferredSize(new Dimension(70,25));
		
		JLabel lblAmplitude2 = new JLabel("Amplitude");
		panel_angamp2.add(lblAmplitude2);
		
		amp_corrente = new JSpinner();		
		amp_corrente.setPreferredSize(new Dimension(70,25));
		panel_angamp2.add(amp_corrente);
		
		JLabel lblordem = new JLabel("Ordem harmônica");
		panel_angamp2.add(lblordem);
		
		ordem_har = new JSpinner();
		ordem_har.setPreferredSize(new Dimension(70,25));
		panel_angamp2.add(ordem_har);

	    
	    //panel da potência instantânea
	    panel_linha3 = new JPanel();
	    GridBagConstraints gbc_panel_linha3 = new GridBagConstraints();
	    gbc_panel_linha3.insets = new Insets(0, 0, 5, 0);
	    gbc_panel_linha3.fill = GridBagConstraints.BOTH;
	    gbc_panel_linha3.gridx = 0;
	    gbc_panel_linha3.gridy = 3;
	    contentPane.add(panel_linha3, gbc_panel_linha3);
	    
	    //panel do gráfico da potência
	    panel_g3 = new JPanel();
	    panel_linha3.add(panel_g3);
	    
	    //gráfico da potência
	    grafico3 = new GraficoPotenciaHarmonico(3, cph);
	    ChartPanel painel_grafico3 = new ChartPanel(grafico3.getChart());
	    panel_g3.add(painel_grafico3);
	    
	    panel_potencias = new JPanel();
	    panel_linha3.add(panel_potencias);
	    
	    //panel valores das potências
	    panel_linha4 = new JPanel();
	    panel_linha4.setPreferredSize(new Dimension(389, 150));
	    GridBagConstraints gbc_panel_linha4 = new GridBagConstraints();
	    gbc_panel_linha4.fill = GridBagConstraints.BOTH;
	    gbc_panel_linha4.gridx = 0;
	    gbc_panel_linha4.gridy = 4;
	    contentPane.add(panel_linha4, gbc_panel_linha4);
	    panel_linha4.setLayout(new GridLayout(0, 2, 0, 0));
	    
	    JLabel lblliquida = new JLabel("Potência líquida");
	    panel_linha4.add(lblliquida);
	    
	    tfliquida = new JTextField();
	    tfliquida.setEditable(false);
	    panel_linha4.add(tfliquida);
	    tfliquida.setColumns(10);
	    
	    JLabel lbldistorcao = new JLabel("Potência de distorção");
	    panel_linha4.add(lbldistorcao);
	    
	    tfdistorcao = new JTextField();
	    tfdistorcao.setEditable(false);
	    panel_linha4.add(tfdistorcao);
	    tfdistorcao.setColumns(10);
	    
	    JLabel lbltpf = new JLabel("TPF");
	    panel_linha4.add(lbltpf);
	    
	    tftpf = new JTextField();
	    tftpf.setEditable(false);
	    panel_linha4.add(tftpf);
	    tftpf.setColumns(10);
	    
	  
	    jsp.setPreferredSize(null);
	    jsp.setBounds(150,670,850,200);
	    setContentPane(jsp);
	    
	    //agora vou definir o que acontece quando o usuário muda o valor do ângulo da tensão
	  	ang_fase_tensao.addChangeListener(new ChangeListener() {
	  		public void stateChanged(ChangeEvent e) {
				atualizar_tensao();
			}
	  	});
	  	//agora vou definir o que acontece quando o usuário muda o valor da amplitude da tensão
		amp_tensao.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				atualizar_tensao();
			}
		});
		//agora vou definir o que acontece quando o usuário muda o valor do ângulo da corrente
		ang_fase_corrente.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				atualizar_corrente();
			}
		});
		//agora vou definir o que acontece quando o usuário muda o valor da amplitude da corrente
		amp_corrente.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				atualizar_corrente();
			}
		});
		//agora vou definir o que acontece quando o usuário muda o valor da ordem harmônica da corrente
		ordem_har.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				atualizar_corrente();
			}
		});
	    
	  		
	}
	
	private void atualizar_tensao() {
		//pegando valores dos spinners
		int amp_ten = (int) amp_tensao.getValue();
		int ang_ten = (int) ang_fase_tensao.getValue();

		
		// mensagem de erro para valores fora dos limites
		if(amp_ten < 0 || amp_ten > 220 || ang_ten < -180 || ang_ten > 180) {
			JOptionPane.showMessageDialog(null, "Valores inválidos\nTensão: 0 ≤ Amplitude ≤ 220\n" + 
					"Ângulo: -180°≤ θ ≤ 180°\n");
			return;
		}
		
		//vou pegar os dados da tensão e montar o novo gráfico
		cph.mudar_amp_tensao(amp_ten);
        cph.mudar_ang_tensao(ang_ten);
		GraficoPotenciaHarmonico grafico_novo1 = new GraficoPotenciaHarmonico(1,cph);
		ChartPanel panel1 = new ChartPanel(grafico_novo1.getChart());
		atualizar_estado(panel_g1, panel1);
	    
		//atualizar a potência com os novos dados
		atualizar_potencia();
		
		
	}
	
	public void atualizar_corrente() {
		int amp_cor = (int) amp_corrente.getValue();
		int ang_cor = (int) ang_fase_corrente.getValue();
		int ord_cor = (int) ordem_har.getValue();
		
		// mensagem de erro para valores fora dos limites
		if(amp_cor < 0 || amp_cor > 100 || ang_cor < -180 || ang_cor > 180 ||
					ord_cor <0 || ord_cor > 15) {
			JOptionPane.showMessageDialog(null, "Valores inválidos\n" + 
					"Corrente: 0 ≤ Amplitude ≤ 100\nÂngulo: -180°≤ θ ≤ 180°\n" +
						"Ordem Harmônica: 0 ≤ h ≤ 15");
			return;
		}

	    //vou pegar os dados da corrente e montar o novo gráfico
        cph.mudar_ang_corrente(ang_cor);		        
        cph.mudar_amp_corrente(amp_cor);
        cph.mudar_ordem_corrente(ord_cor);
		GraficoPotenciaHarmonico grafico_novo2 = new GraficoPotenciaHarmonico(2,cph);
        ChartPanel panel2 = new ChartPanel(grafico_novo2.getChart());
        atualizar_estado(panel_g2, panel2);
        
        //atualizar a potência com os novos dados
        atualizar_potencia();


	}
	
	public void atualizar_potencia () {
        //montar o gráfico da potência
        GraficoPotenciaHarmonico grafico_novo3 = new GraficoPotenciaHarmonico(3,cph);
        ChartPanel panel3 = new ChartPanel(grafico_novo3.getChart());
        atualizar_estado(panel_g3, panel3);
        
        // calcular as potências resultantes
        tfliquida.setText(Double.toString(cph.pegar_potencia_liquida()) + " WATT");
        tfdistorcao.setText(Double.toString(cph.pegar_potencia_distorcao()) + " Volt-Ampére");
        tftpf.setText("0");
		
	}
	
	public void atualizar_estado(JPanel panel, Component comp) {
		panel.removeAll();
		panel.invalidate();								
        panel.add(comp);
        panel.validate();
        panel.repaint();
	}

}
