package views;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;

import javax.swing.JPanel;

@SuppressWarnings("serial")
public class PlanoCartesiano extends JPanel{
	private int potencia_ativa;
	private int potencia_reativa;
	
	public PlanoCartesiano(int potencia_ativa, int potencia_reativa) {
		this.potencia_ativa = potencia_ativa;
		this.potencia_reativa = potencia_reativa;
	}
	
	// constantes das coordenadas X
	 public static final int X_PONTA_ESQUERDA = 0;
	 public static final int TAM_EIXO_X = 400;
	 public static final int EIXO_X_COORD_Y = 200;
	 
	 //constantes das coordenadas Y
	 public static final int Y_PONTA_SUPERIOR = 0;
	 public static final int TAM_EIXO_Y = 400;
	 public static final int EIXO_Y_COORD_X = 200;
	 
	 //setas dos eixos são representados pela hipotenusa do triangulo
	 // agora definimos o compimento dos catetos
	 public static final int TAM_SETAS = 10;
	 public static final int TAM_RISCOS = 5;
	 
	 // tamanho da coordenada inicial
	 public static final int TAM_XY_ZERO = 6;
	 
	 // distância entre strings e eixos
	 public static final int EIXOX_STRING_DISTANCIA  = 20;
	 public static final int EIXOY_STRING_DISTANCIA  = 40;
	 
	 public void paintComponent(Graphics g) {
		  
		  super.paintComponent(g);
		  
		  Graphics2D g2 = (Graphics2D) g;
		  
		  g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
		    RenderingHints.VALUE_ANTIALIAS_ON);
		  
		  // eixo x
		  g2.drawLine(X_PONTA_ESQUERDA, EIXO_X_COORD_Y,
				  	  TAM_EIXO_X, EIXO_X_COORD_Y);
		  // eixo y
		  g2.drawLine(EIXO_Y_COORD_X, Y_PONTA_SUPERIOR,
				  	  EIXO_Y_COORD_X, TAM_EIXO_Y);
		  
		  // seta eixo x
		  g2.drawLine(TAM_EIXO_X - TAM_SETAS,
				      EIXO_X_COORD_Y - TAM_RISCOS,
				      TAM_EIXO_X, EIXO_X_COORD_Y);
		  g2.drawLine(TAM_EIXO_X - TAM_SETAS,
				      EIXO_X_COORD_Y + TAM_RISCOS,
				      TAM_EIXO_X, EIXO_X_COORD_Y);
		  
		  // seta eixo y
		  g2.drawLine(EIXO_Y_COORD_X - TAM_RISCOS,
				      Y_PONTA_SUPERIOR + TAM_SETAS,
				      EIXO_Y_COORD_X, Y_PONTA_SUPERIOR);
		  g2.drawLine(EIXO_Y_COORD_X + TAM_RISCOS, 
				      Y_PONTA_SUPERIOR + TAM_SETAS,
				      EIXO_Y_COORD_X, Y_PONTA_SUPERIOR);
		  
		  // desenha ponto de origem
//		  g2.fillOval(
//		    X_PONTA_ESQUERDA - (TAM_XY_ZERO / 2), 
//		    TAM_EIXO_Y - (TAM_XY_ZERO / 2),
//		    TAM_XY_ZERO, TAM_XY_ZERO);
		  
		  // plota stringS "X" e "Y"
		  g2.drawString("X", TAM_EIXO_X - EIXOX_STRING_DISTANCIA  / 2,
				  		EIXO_X_COORD_Y + EIXOX_STRING_DISTANCIA );
		  g2.drawString("Y", EIXO_Y_COORD_X - EIXOX_STRING_DISTANCIA ,
				  		Y_PONTA_SUPERIOR + EIXOX_STRING_DISTANCIA  / 2);
		  
		  // enumera eixos
		  int XcoordNum = 10;
		  int YcoordNum = 10;
		  int xComprimento = (TAM_EIXO_X - X_PONTA_ESQUERDA)
		      / XcoordNum;
		  int yComprimento = (TAM_EIXO_Y - Y_PONTA_SUPERIOR)
		      / YcoordNum;
		
		  // desenha numeros no eixo x 
		  for(int i = 1; i < XcoordNum; i++) {
			  g2.drawLine(X_PONTA_ESQUERDA + (i * xComprimento),
					  	  EIXO_X_COORD_Y - TAM_RISCOS,
					  	  X_PONTA_ESQUERDA + (i * xComprimento),
					  	  EIXO_X_COORD_Y + TAM_RISCOS);
		   
			  int valor;
			  valor = (i - 5)*4400;			   
		  
			   if (i != 5) {
				   Font font = new Font ("TimesRoman", Font.PLAIN, 9);
				   g2.setFont (font);
				   g2.drawString(Integer.toString(valor), 
						     	 X_PONTA_ESQUERDA + (i * xComprimento) - 3,
						     	 EIXO_X_COORD_Y + EIXOX_STRING_DISTANCIA );
			   }
		   
		  }
		  
		  //desenha numeros no eixo y
		  for(int i = 1; i < YcoordNum; i++) {
			   g2.drawLine(EIXO_Y_COORD_X - TAM_RISCOS,
					   	   TAM_EIXO_Y - (i * yComprimento), 
					   	   EIXO_Y_COORD_X + TAM_RISCOS,
					   	   TAM_EIXO_Y - (i * yComprimento));
			   
			   int valor;
			   valor = (i - 5) * 4400;			   
			  		   
			   if(i!=5) {
				   Font font = new Font ("TimesRoman", Font.PLAIN, 9);
				   g2.setFont (font);
				   g2.drawString(Integer.toString(valor), 
						     	 EIXO_Y_COORD_X - EIXOY_STRING_DISTANCIA , 
						     	 TAM_EIXO_Y - (i * yComprimento));
				   
			   }
		   
		  }
		  
		  //mudando a cor e espessura da linha para fazer vetor resultado
		  g2.setColor(Color.decode("#ff6666"));
		  g2.setStroke(new BasicStroke(3));
		  
		  // desenha vetor da potencia ativa no eixo x
		  int v1 = potencia_ativa * 2/220;		  
		  g2.drawLine(EIXO_Y_COORD_X, EIXO_X_COORD_Y,
				     EIXO_Y_COORD_X + v1, EIXO_X_COORD_Y);
		  
		  // desenha vetor da potencia reativa no eixo y
		  int v2 = potencia_reativa * 2/220;
		  int y = EIXO_Y_COORD_X - v2;
		  g2.drawLine(EIXO_Y_COORD_X + v1, EIXO_X_COORD_Y,
				     EIXO_Y_COORD_X + v1, y);
		  
		  //desenha vetor da potencia aparente
		  g2.drawLine( EIXO_Y_COORD_X, EIXO_X_COORD_Y,
				     EIXO_Y_COORD_X + v1, y);
		  
		  
		  
	
	}
	 
	 
}
