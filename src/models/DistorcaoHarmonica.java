package models;

import java.util.ArrayList;
import java.util.List;

public class DistorcaoHarmonica {
	private int ang_fase_comp_fund;
	private int amplitude_comp_fund;
	private String paridade;
	private List<Harmonicas> ordens_harmonicas;
	private static final double w = 2*3.1415*60;
	
	public DistorcaoHarmonica() {
		this.ang_fase_comp_fund = 0;
		this.amplitude_comp_fund = 0;
		this.ordens_harmonicas = new ArrayList<>();
	}
	
	//gets e sets necessários
	public int getAngFaseCompFund() {
		return ang_fase_comp_fund;
	}
	public void setAngFaseCompFund(int ang_fase_comp_fund) {
		this.ang_fase_comp_fund = ang_fase_comp_fund;
	}
	
	public String getParidade() {
		return paridade;
	}
	public void setParidade(String paridade) {
		this.paridade = paridade;
	}
	
	public List<Harmonicas> getOrdensHarmonicas() {
		return ordens_harmonicas;
	}
	// esse set recebe o numero de harmonicos, cria objetos do tipo Harmonicas e
	// armazena cada um na lista que é um atributo dessa classe
	public void setOrdensHarmonicas(int num_harmonicas) {
		ordens_harmonicas = new ArrayList<>();
		for (int i = 0; i < num_harmonicas; i++) {
			Harmonicas harmonicas = new Harmonicas();
			ordens_harmonicas.add(harmonicas);
		}
	}
	
	public int getAmplitudeCompFund() {
		return amplitude_comp_fund;
	}
	public void setAmplitudeCompFund(int amplitude_comp_fund) {
		this.amplitude_comp_fund = amplitude_comp_fund;
	}
	
	// aplica x nas funções correspondentes e acha y
	// gera lista de pontos x/y	
	public List<Pontos> resolver_comp_fund() {
		List<Pontos> pontos = new ArrayList<Pontos>();
		for (int i = 0; i < 50; i++) {
			double x = i/1000.0;
			double y = amplitude_comp_fund*Math.cos(w*x + Math.toRadians(ang_fase_comp_fund));
			Pontos pt = new Pontos(x,y);
			pontos.add(pt);
			
		}
		return pontos;
		
	}
	
	public List<Pontos> resolver_ordem_harmonica(int index) {
		List<Pontos> pontos = new ArrayList<Pontos>();
		for (int i = 0; i < 50; i++) {
			double x = i/1000.0;
			double y = ordens_harmonicas.get(index).getAmplitude()*Math.cos(ordens_harmonicas.get(index).getOrdem()*w*x + Math.toRadians(ordens_harmonicas.get(index).getAngFase()));
			Pontos pt = new Pontos(x,y);
			pontos.add(pt);
			
		}
		return pontos;
		
	}
	
	public List<Pontos> resolver_fourrier() {
		List<Pontos> pontos = new ArrayList<Pontos>();
		for (int i = 0; i < 50; i++) {
			double x = i/1000.0;
			double y0 = amplitude_comp_fund*Math.cos(w*x + Math.toRadians(ang_fase_comp_fund));
			double y1 = 0;
			for (int j = 0; j < ordens_harmonicas.size(); j++) {
				y1 += ordens_harmonicas.get(j).getAmplitude()*Math.cos(ordens_harmonicas.get(j).getOrdem()*w*x + Math.toRadians(ordens_harmonicas.get(j).getAngFase())); ;
			}
			double y = y0 + y1;
			Pontos pt = new Pontos(x,y);
			pontos.add(pt);
			
		}
		return pontos;
		
	}
	
	public String getSerieFourrier() {
		String serie;
		String s0 = amplitude_comp_fund + "cos(wt + " + ang_fase_comp_fund + ")";
		String s1 = "";
		for (int i = 0; i < ordens_harmonicas.size(); i++) {
			if(i==4) {
				s1+= "\n";
			}
			if(ordens_harmonicas.get(i).getAmplitude() == 0) {
				s1+= "";
			} else {
				if(ordens_harmonicas.get(i).getOrdem() == 0) {
					s1 += " + " + ordens_harmonicas.get(i).getAmplitude() + "cos(" + ordens_harmonicas.get(i).getAngFase() + ")";
				} else if(ordens_harmonicas.get(i).getAngFase() == 0) {
					s1 += " + " + ordens_harmonicas.get(i).getAmplitude() + "cos(" + ordens_harmonicas.get(i).getOrdem() + "wt)";
				} else {
					s1 += " + " + ordens_harmonicas.get(i).getAmplitude() + "cos(" + ordens_harmonicas.get(i).getOrdem() + "wt + " + ordens_harmonicas.get(i).getAngFase() + ")";
				}
				
			}
			
		}
		serie = "f(t) = " + s0 + s1;
		return serie;
	}

}
