package models;

import java.util.ArrayList;
import java.util.List;

public class PotenciaHarmonico {
	private int ang_tensao;
	private int amp_tensao;
	private int ang_corrente;
	private int amp_corrente;
	private int ordem_corrente;
	private double potencia_liquida;
	private double potencia_distorcao;
	private double tpf;
	private static final double w = 2*3.1415*60;
	
	public PotenciaHarmonico() {
		 ang_tensao = 0;
		 amp_tensao = 0;
		 ang_corrente = 0;
		 amp_corrente = 0;
		 ordem_corrente = 0;
		 potencia_liquida = 0;
		 potencia_distorcao = 0;
		 tpf = 0;
		
	}
	
	// gets e sets necessários
	public int getAngTensao() {
		return ang_tensao;
	}
	public void setAngTensao(int ang_tensao) {
		this.ang_tensao = ang_tensao;
	}
	public int getAmpTensao() {
		return amp_tensao;
	}
	public void setAmpTensao(int amp_tensao) {
		this.amp_tensao = amp_tensao;
	}
	public int getAngCorrente() {
		return ang_corrente;
	}
	public void setAngCorrente(int ang_corrente) {
		this.ang_corrente = ang_corrente;
	}
	public int getAmpCorrente() {
		return amp_corrente;
	}
	public void setAmpCorrente(int amp_corrente) {
		this.amp_corrente = amp_corrente;
	}
	public int getOrdemCorrente() {
		return ordem_corrente;
	}
	public void setOrdemCorrente(int ordem_corrente) {
		this.ordem_corrente = ordem_corrente;
	}
	
	//calcula potencias
	public double getPotenciaLiquida() {
		return potencia_liquida;
	}
	public double getPotenciaDistorcao() {
		potencia_distorcao = amp_tensao*amp_corrente;
		return potencia_distorcao;
	}
	public double getTpf() {
		return tpf;
	}

	// aplica x nas funções correspondentes e acha y
	// gera lista de pontos x/y
	public List<Pontos> resolver_tensao() {
		List<Pontos> pontos = new ArrayList<Pontos>();
		for (int i = 0; i < 50; i++) {
			double x = i/1000.0;
			double y = amp_tensao*Math.cos(w*x + Math.toRadians(ang_tensao));
			Pontos pt = new Pontos(x,y);
			pontos.add(pt);
			
		}
		return pontos;
		
	}
	
	public List<Pontos> resolver_corrente_harmonica() {
		List<Pontos> pontos = new ArrayList<Pontos>();
		for (int i = 0; i < 50; i++) {
			double x = i/2000.0;
			double y = amp_corrente*Math.cos(ordem_corrente*w*x + Math.toRadians(ang_corrente));
			Pontos pt = new Pontos(x,y);
			pontos.add(pt);
			
		}
		return pontos;
		
	}
	

	public List<Pontos> resolver_potencia_instantanea(){
		List<Pontos> pontos = new ArrayList<>();
		for (int i = 0; i < 70; i++) {
			double x = i/3000.0;
			double y = (amp_tensao*Math.cos(w*x + Math.toRadians(ang_tensao))) * (amp_corrente*Math.cos(ordem_corrente*w*x + Math.toRadians(ang_corrente)));
			Pontos pt = new Pontos(x,y);
			pontos.add(pt);
		}
		
		return pontos;
	}
	
	
	

}
