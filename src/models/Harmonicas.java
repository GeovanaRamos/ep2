package models;

public class Harmonicas {
	private int ang_fase;
	private int ordem;
	private int amplitude;
	
	public Harmonicas() {
		this.ang_fase = 0;
		this.ordem = 0;
		this.amplitude = 0;
	}
	
	public int getAngFase() {
		return ang_fase;
	}
	public void setAngFase(int ang_fase) {
		this.ang_fase = ang_fase;
	}
	
	public int getOrdem() {
		return ordem;
	}
	public void setOrdem(int ordem) {
		this.ordem = ordem;
	}
	public int getAmplitude() {
		return amplitude;
	}
	public void setAmplitude(int amplitude) {
		this.amplitude = amplitude;
	}
	
	
}
