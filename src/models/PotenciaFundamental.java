package models;

import java.util.ArrayList;
import java.util.List;

public class PotenciaFundamental {
	private int amplitude_corrente;
	private int angulo_de_fase;
	private int amplitude_tensao;
	private int angulo_defasagem_corrente;
	private double potencia_ativa;
	private double potencia_reativa;
	private double potencia_aparente;
	private double fator_de_potencia;
	private static final double w = 2*3.1415*60;
	
	public PotenciaFundamental () {
		amplitude_tensao = 0;
		angulo_de_fase = 0;
		amplitude_corrente = 0;
		angulo_defasagem_corrente = 0;
		
	}
	
	//gets e sets necessários
	
	public int getAmplitudeCorrente() {
		return amplitude_corrente;
	}
	public void setAmplitudeCorrente(int amplitude_corrente) {
		this.amplitude_corrente = amplitude_corrente;
	}
	
	public int getAnguloDeFase() {
		return angulo_de_fase;
	}
	public void setAnguloDeFase(int angulo_de_fase) {
		this.angulo_de_fase = angulo_de_fase;
	}
	
	public int getAmplitudeTensao() {
		return amplitude_tensao;
	}
	public void setAmplitudeTensao(int amplitude_tensao) {
		this.amplitude_tensao = amplitude_tensao;
	}
	
	public int getAnguloDefasagemCorrente() {
		return angulo_defasagem_corrente;
	}
	public void setAnguloDefasagemCorrente(int angulo_defasagem_corrente) {
		this.angulo_defasagem_corrente = angulo_defasagem_corrente;
	}
	
	//jogar valores de x nas funções e calcular y
	// gerar lista de pontos x/y
	
	public List<Pontos> resolver_onda_tensao() {
		List<Pontos> pontos = new ArrayList<Pontos>();
		for (int i = 0; i < 50; i++) {
			double x = i/1000.0;
			double y = amplitude_tensao*Math.cos(w*x + Math.toRadians(angulo_de_fase));
			Pontos pt = new Pontos(x,y);
			pontos.add(pt);
			
		}
		return pontos;
		
	}
	
	public List<Pontos> resolver_onda_corrente() {
		List<Pontos> pontos = new ArrayList<Pontos>();
		for (int i = 0; i < 50; i++) {
			double x = i/1000.0;
			double y = amplitude_corrente*Math.cos(w*x + Math.toRadians(angulo_defasagem_corrente));
			Pontos pt = new Pontos(x,y);
			pontos.add(pt);
			
		}
		return pontos;
		
	}
	
	public List<Pontos> resolver_potencia_instantanea(){
		List<Pontos> pontos = new ArrayList<>();
		for (int i = 0; i < 60; i++) {
			double x = i/3000.0;
			double y = (amplitude_tensao*Math.cos(w*x + Math.toRadians(angulo_de_fase))) * (amplitude_corrente*Math.cos(w*x + Math.toRadians(angulo_defasagem_corrente)));
			Pontos pt = new Pontos(x,y);
			pontos.add(pt);
		}
		
		return pontos;
	}
	
	// calcular potências
	
	public double getPotenciaAtiva() {
		potencia_ativa = amplitude_tensao * amplitude_corrente * Math.cos(Math.toRadians(angulo_de_fase)- Math.toRadians(angulo_defasagem_corrente));
		return potencia_ativa;
	}
	
	public double getPotenciaReativa() {
		potencia_reativa = amplitude_tensao * amplitude_corrente * Math.sin(Math.toRadians(angulo_de_fase)- Math.toRadians(angulo_defasagem_corrente));
		return potencia_reativa;
	}
	
	public double getPotenciaAparente() {
		potencia_aparente = amplitude_tensao * amplitude_corrente;
		return potencia_aparente;
	}
	
	public double getFatorDePotencia() {
		fator_de_potencia= Math.cos(Math.toRadians(angulo_de_fase - angulo_defasagem_corrente));
		return fator_de_potencia;
	}
	

	
	
}
