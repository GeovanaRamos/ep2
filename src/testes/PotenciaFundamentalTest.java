package testes;

import static org.junit.Assert.*;

import org.junit.Test;
import models.PotenciaFundamental;


public class PotenciaFundamentalTest {

	
	PotenciaFundamental pf = new PotenciaFundamental();
	
	@Test
	public void testPotenciaAtiva() {
		pf.setAmplitudeTensao(220);
		pf.setAnguloDeFase(0);
		pf.setAmplitudeCorrente(39);
		pf.setAnguloDefasagemCorrente(35);
		assertEquals(7028, (int) pf.getPotenciaAtiva());
	}
	
	@Test
	public void testPotenciaReativa() {
		pf.setAmplitudeTensao(220);
		pf.setAnguloDeFase(0);
		pf.setAmplitudeCorrente(39);
		pf.setAnguloDefasagemCorrente(35);
		assertEquals(-4921, (int) pf.getPotenciaReativa());
	}
	
	@Test
	public void testPotenciaAparente() {
		pf.setAmplitudeTensao(220);
		pf.setAnguloDeFase(0);
		pf.setAmplitudeCorrente(39);
		pf.setAnguloDefasagemCorrente(35);
		assertEquals(8580, (int) pf.getPotenciaAparente());
	}
	
	@Test
	public void testFatorDePotencia() {
		pf.setAmplitudeTensao(220);
		pf.setAnguloDeFase(0);
		pf.setAmplitudeCorrente(39);
		pf.setAnguloDefasagemCorrente(35);
		assertEquals(0.82, pf.getFatorDePotencia(), 2);
	}
}
