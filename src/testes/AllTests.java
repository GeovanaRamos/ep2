package testes;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ DistorcaoHarmonicaTest.class, PotenciaFundamentalTest.class, PotenciaHarmonicoTest.class })
public class AllTests {

}
