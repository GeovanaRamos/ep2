package testes;

import static org.junit.Assert.*;

import org.junit.Test;

import models.PotenciaHarmonico;

public class PotenciaHarmonicoTest {

	PotenciaHarmonico ph = new PotenciaHarmonico();
	
	@Test
	public void testPotenciaDeDistorcao() {
		ph.setAmpTensao(220);
		ph.setAmpCorrente(39);
		assertEquals(8580, (int) ph.getPotenciaDistorcao());
	}
	

}
