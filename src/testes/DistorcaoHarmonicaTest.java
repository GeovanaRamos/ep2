package testes;

import static org.junit.Assert.*;

import org.junit.Test;

import models.DistorcaoHarmonica;

public class DistorcaoHarmonicaTest {
	
	DistorcaoHarmonica dh = new DistorcaoHarmonica();

	@Test
	public void testSerieDeFourrier() {
		dh.setOrdensHarmonicas(2);
		dh.setAmplitudeCompFund(220);
		dh.setAngFaseCompFund(0);
		dh.getOrdensHarmonicas().get(0).setAmplitude(20);
		dh.getOrdensHarmonicas().get(0).setAngFase(30);
		dh.getOrdensHarmonicas().get(0).setOrdem(3);
		dh.getOrdensHarmonicas().get(1).setAmplitude(15);
		dh.getOrdensHarmonicas().get(1).setAngFase(-90);
		dh.getOrdensHarmonicas().get(1).setOrdem(5);
		assertEquals("f(t) = 220cos(wt + 0) + 20cos(3wt + 30) + 15cos(5wt + -90)", dh.getSerieFourrier());
		
	}

}
